import React from 'react';
import { connect } from 'react-redux';
import DishProfile from '../views/DishProfile';
import store from '../../store';
import * as detailDish from '../../actions/DishAcions';
import getConfigParseFunctions from '../../config/configParse';

const DishProfileContainer = React.createClass({
  componentWillMount: function() {
    console.log('get data dish class');
    console.log(this.props.params.userId);
    getConfigParseFunctions.getDishObject("Dish", this.props.params.userId).then(
        function(object) {
        console.log('get data success');
        console.log(object);
        store.dispatch(detailDish.dishProfileSuccess(object));
        },
        function(error) {
            response.error(error);
        }
    );
  },
  render: function() {
    if((typeof (this.props.profile) !== 'undefined') && this.props.profile.data){
      console.log('render to user profile container');
      console.log(this.props.profile);
      return (
         <DishProfile dishProfile={this.props.profile.data}/>
      );
    } else {
      return null;
    }
  }

});

const mapStateToProps = function(store) {
  console.log('get state in user profile container');
  console.log(store.dishState.dishProfile);
  return {
    profile: store.dishState.dishProfile,
  };
};

export default connect(mapStateToProps)(DishProfileContainer);
