import React, { PropTypes, Component } from 'react';
import { connect } from 'react-redux';
import EditDish from '../views/EditDish';
import store from '../../store';
import * as detailDish from '../../actions/DishAcions';
import getConfigParseFunctions from '../../config/configParse';
import Parse from 'parse';
export default class EditDishContainer extends Component {
  constructor (props) {
    super(props);
    console.log('constructor');
    console.log(props);
    this.state = {
      dishform: '',
      dish: '',
      files: [],
      link: '',
      recipe: '',
      description: '',
      type: '',
    };
    this.handleDishNameChange = this.handleDishNameChange.bind(this);
    this.handleImageDrop = this.handleImageDrop.bind(this);
    // this.handleOpenClickImage = this.handleOpenClickImage.bind(this);
    this.handleLinkChange = this.handleLinkChange.bind(this);
    this.handleRecipeChange = this.handleRecipeChange.bind(this);
    this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
    this.handleTypesChange = this.handleTypesChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleDishNameChange(evt){
    evt.preventDefault();
    // console.log('handleDishNameChange');
    // console.log(evt);
    this.setState({
      dish: evt.target.value,
    });
  }
  handleImageDrop(files){
    // console.log('handleImageChange');
    // console.log(files);
    this.setState({
      files: files
    });
  }
  handleOpenClickImage(e){
    // console.log('handleOpenClickImage');
    // console.log(e);
    // this.refs.fileInput.getDOMNode().click();
    // console.log(this.ref);
    this.props.refs.dropzone.open();
  }
  handleLinkChange(evt){
    evt.preventDefault();
    this.setState({
      link: evt.target.value,
    });
  }
  handleRecipeChange(evt){
    evt.preventDefault();
    this.setState({
      recipe: evt.target.value,
    });
  }
  handleDescriptionChange(evt){
    evt.preventDefault();
    this.setState({
      description: evt.target.value,
    });
  }
  handleTypesChange(evt){
    // console.log('handleTypesChange');
    // console.log(evt);
    this.setState({
      type: evt.target.value,
    });
  }
  handleSubmit(e){
    e.preventDefault();
    // console.log('handleSubmit');
    // console.log(this.state.dish.trim());
    // console.log(this.state.link.trim());
    // console.log(this.state.recipe.trim());
    // console.log(this.state.description.trim());
    // console.log(this.state.type.trim());

    let type;
    if (this.state.type.trim() === "1") {
      type = "Central";
    } else if(this.state.type.trim() === "2") {
      type = "Nothern";
    } else {
      type = "Southern";
    }
    // console.log(type);
    let newDish = {
      name: this.state.dish.trim(),
      link: this.state.link.trim(),
      recipe: this.state.recipe.trim(),
      description: this.state.description.trim(),
      type: type,
    }
    var file = this.state.files[0];
    var userId = this.props.params.userId;
    // console.log(userId);
    var serverUrl = 'https://api.parse.com/1/files/' + file.name;
    $.ajax({
      type: "POST",
      beforeSend: function(request) {
        request.setRequestHeader("X-Parse-Application-Id", 'xh7e2OhXMAsuZif0ZuSjtKUZeSGP0quEIU9s1VWF');
        request.setRequestHeader("X-Parse-REST-API-Key", 'T6XYu7rzzcuiZFinDLcMXYtKY8y3xZOQLn6NoGif');
        request.setRequestHeader("Content-Type", file.type);
      },
      url: serverUrl,
      data: file,
      processData: false,
      contentType: false,
      success: function(data) {
        // console.log("File available at: " + data.url);
        var data = {image: {uri: data.url}};
        newDish = _.assignIn(newDish, data);
        // console.log(newDish);
        uploadImageToParse(newDish, userId);
      },
      error: function(data) {
        var obj = jQuery.parseJSON(data);
        alert(obj.error);
      }
    });
  }
  componentWillReceiveProps (nextProps) {
    // console.log('componentWillReceiveProps');
    // console.log(nextProps);
    this.setState({
      dishform: (nextProps.form && nextProps.form.data) ? nextProps.form.data : '',
      dish: (nextProps.form && nextProps.form.data) ? nextProps.form.data.attributes.name : '',
      files: (nextProps.form && nextProps.form.data) ? [{preview: nextProps.form.data.attributes.image.uri}] : [],
      link: (nextProps.form && nextProps.form.data) ? nextProps.form.data.attributes.link : '',
      recipe: (nextProps.form && nextProps.form.data) ? nextProps.form.data.attributes.recipe : '',
      description: (nextProps.form && nextProps.form.data) ? nextProps.form.data.attributes.description : '',
      type: (nextProps.form && nextProps.form.data) ? nextProps.form.data.attributes.type : '',
    });
  }
  componentWillMount() {
    // console.log('get data dish class');
    // console.log(this.props.params.userId);
    getConfigParseFunctions.getDishObject("Dish", this.props.params.userId).then(
        function(object) {
        // console.log('get data success');
        // console.log(object);
        store.dispatch(detailDish.dishProfileSuccess(object));
        },
        function(error) {
            response.error(error);
        }
    );
  }
  render() {
    if (this.state.dishform) {
      // console.log('render to EditDish views');
      // console.log(this.state.files);
      return (
         <EditDish
          dishform={this.state.dishform}
          dish={this.state.dish}
          files={this.state.files}
          link={this.state.link}
          recipe={this.state.recipe}
          description={this.state.description}
          type={this.state.type}
          onHandleDishName={(e) => this.handleDishNameChange(e)}
          onHandleDrop={(files) => this.handleImageDrop(files)}
          onHandleLinkName={(e) => this.handleLinkChange(e)}
          onHandleRecipe={(e) => this.handleRecipeChange(e)}
          onHandleDescription={(e) => this.handleDescriptionChange(e)}
          onHandleSelection={(e) => this.handleTypesChange(e)}
          onHandleSubmit={(e) => this.handleSubmit(e)}
          />
      );
    } else {
      console.log('render null');
      return null;
    }
  }
}

function uploadImageToParse(newDish, userId) {
  // console.log('uploadImageToParse func');
  getConfigParseFunctions.findDishObject("Dish", userId).then(
      function(object) {
      // console.log('get data success');
      // console.log(object);
      // console.log(object.get("name"));
      // console.log(newDish.name);
      object.set("name", newDish.name);
      object.set("link", newDish.link);
      object.set("recipe", newDish.recipe);
      object.set("description", newDish.description);
      object.set("type", newDish.type);
      object.set("image", newDish.image);
      object.save();
      // console.log(object);
      store.dispatch(detailDish.updateDishSuccess(object));
      },
      function(error) {
          response.error(error);
      }
  );
}
const mapStateToProps = function(state) {
  // console.log('get state in user form container');
  // console.log(state.form.dishProfile);
  return {
    form: state.form.dishProfile,
  };
};

export default connect(mapStateToProps)(EditDishContainer);
