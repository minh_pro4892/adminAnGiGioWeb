import React from 'react';
import Parse from 'parse';
import ParseReact from 'parse-react';
import { connect } from 'react-redux';
import DishList from '../../layouts/dishes/DishList';
import getConfigParseFunctions from '../../../config/configParse';
import store from '../../../store';
import { loadSearchLayout } from '../../../actions/search-layout-actions';
import * as detailDish from '../../../actions/DishAcions';
import SearchInput, {createFilter} from 'react-search-input';
const KEYS_TO_FILTERS = ['name', 'description', 'recipe']
const DishListContainer = React.createClass({
  getInitialState: function() {
      return { searchTerm: '' }
  },
  searchUpdated: function(term) {
     this.setState({searchTerm: term})
  },
  componentWillMount: function() {
    getConfigParseFunctions.getClass("Dish").then(
        function(data) {
          store.dispatch(detailDish.getDishesSuccess(data));
        },
        function(error) {
            response.error(error);
        }
    );
  },
  componentWillReceiveProps: function(nextProps) {
    // console.log('componentWillReceiveProps');
    // console.log(nextProps);
    nextProps.dispatch(loadSearchLayout('dishes', 'dishes Results'));
  },
  render: function() {
    let searchInputStyle = {
      width: 400,
      height: 200,
      padding: 10,
    };

    if (this.props.dishes && this.props.dishes[0]) {
      const filteredEmails = this.props.dishes[0].data.filter(createFilter(this.state.searchTerm, KEYS_TO_FILTERS))
      // console.log(KEYS_TO_FILTERS);
      // console.log('get result search');
      // console.log(filteredEmails);
      return (
         <div>
           <SearchInput className="search-input" style={searchInputStyle} onChange={this.searchUpdated} />
           <DishList dishes={filteredEmails} store={store} detailDish={detailDish}></DishList>
         </div>
       )
    } else {
      return null;
    }
  }

});

const mapStateToProps = function(state, ownProps) {
  return {
    dishes: state.dishState.dishes
  };
};

export default connect(
  mapStateToProps,
  // mapDispatchToProps
  )(DishListContainer);
