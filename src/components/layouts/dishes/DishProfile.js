import React from 'react';

// Using "Stateless Functional Components"
export default function(props) {
  console.log('user profile views');
  console.log(props);
  return (
    <div className="user-profile" key={props.dishProfile.id}>
      <img src={props.dishProfile.attributes.image.uri} />
      <div className="details">
        <h1>{props.dishProfile.attributes.name}</h1>
        <a href={props.dishProfile.attributes.link}>{props.dishProfile.attributes.link}</a>
        <p>Recipe<strong>{props.dishProfile.attributes.recipe}</strong></p>
      </div>
    </div>
  );
}
