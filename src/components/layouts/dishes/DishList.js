import React from 'react';
import Parse from 'parse';
import { Link } from 'react-router';
import getConfigParseFunctions from '../../../config/configParse';
export default function(props) {
  // console.log('user list views');
  // console.log(props);
  return (
    <div className="data-list">
    {props.dishes.map((dish, index) => {
      return (
        <div key={index} className="data-list-item">
          <div className="details" data={dish}>
            <Link to={'/dishes/' + dish.id} >{dish.attributes.name}</Link>
          </div>
          <div className="controls" data={dish}>
            <Link to={'/dishes/edit/' + dish.id} >Edit</Link>
          </div>
          <div className="controls">
            <button  onClick={() => deleteDish(props, dish.id)} className="delete">Delete</button>
          </div>
        </div>
      );
    })}
      <footer className="search-footer">
        {props.dishes.length} Results
      </footer>
    </div>
  );
}

function deleteDish(props, dishId) {
  props.store.dispatch(props.detailDish.deleteDishesuccess(dishId));
  deleteDishInParse(dishId);
}

function deleteDishInParse(id) {
  getConfigParseFunctions.getDishObject("Dish", id).then(
      function(object) {
          object.destroy({});
      },
      function(error) {
          response.error(error);
      }
  );
}
