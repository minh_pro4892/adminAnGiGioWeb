import React from 'react';
import Dropzone from 'react-dropzone';
// import { Field, reduxForm } from 'redux-form';
// Using "Stateless Functional Components"
export default (props) => {
  // console.log('get props in form container');
  // console.log(props);
  const {onHandleDishName, onHandleDrop, onHandleLinkName, onHandleRecipe, onHandleDescription, onHandleSelection, onHandleSubmit} = props;
  let inputStyle = {
    width: 400,
    height: 40,
  };
  let imageStyle = {
    width: 200,
    height: 150,
  };
  let textareaStyle = {
    width: 400,
    height: 100,
  };
    return (
      <form onSubmit={(e) => onHandleSubmit(e)} encType="multipart/form-data">
        <div>
          <label>Name</label>
          <div>
            <input name="dish" style={inputStyle} type="text" placeholder="Dish" value={props.dish} onChange={(e) => onHandleDishName(e)}/>
          </div>
        </div>
        <div>
          <label>Image</label>
          <div>
            <Dropzone ref="dropzone" onDrop={(files) => onHandleDrop(files)} >
              <div>Try dropping some files here, or click to select files to upload.</div>
              <div>{props.files.map((file, index) => <img style={imageStyle} key={index} src={file.preview} /> )}</div>
            </Dropzone>
          </div>
        </div>
        <div>
          <label>Link</label>
          <div>
            <input name="link" style={inputStyle} type="text" placeholder="Link" value={props.link} onChange={(e) => onHandleLinkName(e)}/>
          </div>
        </div>
        <div>
          <label>Recipe</label>
          <div>
            <textarea name="recipe" style={textareaStyle} type="text" placeholder="Recipe" value={props.recipe} onChange={(e) => onHandleRecipe(e)}/>
          </div>
        </div>
        <div>
          <label>Description</label>
          <div>
            <textarea name="description" style={textareaStyle} type="text" placeholder="Description" value={props.description} onChange={(e) => onHandleDescription(e)}/>
          </div>
        </div>
        <div>
          <label>Types</label>
            <div>
             <select name="types" style={inputStyle} onChange={(e) => onHandleSelection(e)}>
               <option value="0">{props.type}</option>
               <option value="1">Central</option>
               <option value="2">Northern</option>
               <option value="3">Southern</option>
             </select>
            </div>
          </div>
          <div>
             <button type="submit"> Update Dish</button>
          </div>
      </form>
    )
}
