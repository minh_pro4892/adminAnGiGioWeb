import * as types from '../actions/ActionTypes';

export function getDishesSuccess(dishes) {
  // console.log('getdishesSuccess');
  // console.log(dishes);
  return {
    type: types.GET_DISH_SUCCESS,
    dishes
  };
}

export function updateDishSuccess(dish) {
  console.log('updateDishSuccess');
  console.log(dish);
  return {
    type: types.UPDATE_DISH_SUCCESS,
    dish
  };
}

export function deleteDishesuccess(dishId) {
  return {
    type: types.DELETE_DISH_SUCCESS,
    dishId
  };
}

export function dishProfileSuccess(dishProfile) {
  return {
    type: types.DISH_PROFILE_SUCCESS,
    dishProfile
  };
}
