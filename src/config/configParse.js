const configParse ={
    applicationId: 'xh7e2OhXMAsuZif0ZuSjtKUZeSGP0quEIU9s1VWF',
    restapiKey: 'T6XYu7rzzcuiZFinDLcMXYtKY8y3xZOQLn6NoGif',
    javascriptKey: 'nb2uBbkPm5HNnZrsqxKtqXebLAg8mVknszafJd5V',
    host: 'https://api.parse.com/1'
};

var ParseReact = require('parse');
ParseReact.initialize( configParse.applicationId, configParse.javascriptKey );
ParseReact.serverURL = configParse.host;

const getClass = (name) => {
  let query = new ParseReact.Query(name);
  return query.find();
}

const getDishObject = (_class, _id) => {
  let query = new ParseReact.Query(_class);
  return query.get(_id);
}

const findDishObject = (_class, _id) => {
  let query = new ParseReact.Query(_class);
  query.equalTo("objectId", _id);
  return query.first();
}

export default {
    getClass,
    getDishObject,
    findDishObject,
}
