import * as types from '../actions/ActionTypes';
import _ from 'lodash';
const initialState = {
  dishProfile: {},
};

const FormReducer = function(state = initialState, action) {
  // console.log('detail dishes in form reducers');
  // console.log(action);
  switch(action.type) {
    case types.DISH_PROFILE_SUCCESS:
      return Object.assign({}, state, {
          dishProfile: Object.assign({}, state.dishProfile, { data: action.dishProfile }),
      });
    case types.UPDATE_DISH_SUCCESS:
      return Object.assign({}, state, {
          dishProfile: Object.assign({}, state.dishProfile, { data: action.dish }),
      });
    default:
      return state;
  }

}

export default FormReducer;
