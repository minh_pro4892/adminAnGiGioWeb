import * as types from '../actions/ActionTypes';
import _ from 'lodash';
const initialState = {
  dishes: [],
  dishProfile: {},
};

const DishReducer = function(state = initialState, action) {
  // console.log('detail dishes in dish reducers');
  // console.log(action);
  switch(action.type) {

    case types.GET_DISH_SUCCESS:
      return Object.assign({}, state, {
        dishes: [
        Object.assign({}, state.dishes, { data: action.dishes}),
       ]
      });
    case types.DELETE_DISH_SUCCESS:
    // console.log(state.dishes[0].data.length);
    // console.log(state.dishes[0].data);
    // let deletedUser = state.dishes[0].data;
      // Use lodash to create a new user array without the user we want to remove
      const deletedDish = _.filter(state.dishes[0].data, dish => dish.id !== action.dishId);
      // console.log(deletedDish.length);

      return Object.assign({}, state, {
        dishes: [
        Object.assign({}, state.dishes, { data: deletedDish}),
       ]
      });
    case types.DISH_PROFILE_SUCCESS:
      return Object.assign({}, state, {
          dishProfile: Object.assign({}, state.dishProfile, { data: action.dishProfile }),
      });
    default:
        return state;
  }

}

export default DishReducer;
