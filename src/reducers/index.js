import { combineReducers } from 'redux';
// import { reducer as formReducer } from 'redux-form';
// Reducers
import DishReducer from './DishReducer';
import FormReducer from './FormReducer';
import searchLayoutReducer from './search-layout-reducer';

// Combine Reducers
const reducers = combineReducers({
    dishState: DishReducer,
    form: FormReducer,
    searchLayoutState: searchLayoutReducer
});

export default reducers;
